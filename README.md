# alpine-nodejs
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-nodejs)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-nodejs)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-nodejs/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-nodejs/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Node.js](https://nodejs.org/)
    - Node.js® is a JavaScript runtime built on Chrome's V8 JavaScript engine.



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/alpine-nodejs:[ARCH_TAG]
```



----------------------------------------
#### Usage

```
# node
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

